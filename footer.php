<footer>
  <section class="top-menu">
    <div class="wrapper">
      <?php include('partials/footerNav.php'); ?>
    </div>
  </section>
  <section class="footer-copyrights">
    <p>GPS LLC © 2017 | All Rights Reserved</p>
    <a href="mailto:studio@dillcom.pl">Created by Dillcom</a>
  </section>
</footer>
<?php wp_footer() ?>
</body>

</html>
