<?php
/**
 * Template Name: Contact
 */
get_header();
?>
  <section id="map">

  </section>
  <section class="contact">
    <div class="wrapper">
      <div class="contact-box">
        <div class="contact-form">
          <h2>Write us</h2>
          <?php echo do_shortcode( '[contact-form-7 id="214" title="Form"]' ); ?>
        </div>
        <div class="contact-info">
          <div class="adress">
            <span class="marker-icon"><img src="<?php echo get_template_directory_uri(); ?>/img/location-icon.png" alt=""></span>
            <h3>Adress</h3>
            <p>Global Pollution Services</p>
            <p>Inc. PO Box 134, Lake Charles</p>
            <p>LA 70602 USA</p>
          </div>
          <div class="email">
            <span class="mail-icon"><img src="<?php echo get_template_directory_uri(); ?>/img/e-mail-icon.png" alt=""></span>
            <h3>E-mail adresses</h3>
            <a href="mailto:office@gps-la.com">office@gps-la.com</a>
          </div>
        </div>
      </div>
    </div>
  </section>

  <?php get_footer() ?>
