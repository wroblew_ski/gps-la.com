<nav id="main-nav" class="main-nav">
  <div class="nav-wrapper">
    <div class="nav-box">
      <div class="logo-box">
        <a href="<?php echo get_page_link(5); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" alt="logo"></a>
      </div>
      <div class="navbar-container">
        <div id="nav-icon">
          <span></span>
          <span></span>
          <span></span>
          <span></span>
        </div>
        <ul class="secondary-nav">
          <li><a href="<?php echo get_page_link(10); ?>">About us</a></li>
          <li><a href="<?php echo get_page_link(12); ?>">Contact us</a></li>
        </ul>
        <?php
        $args = array (
          'container_class' => 'navbar-container',
          'menu_class' => 'primary-nav',
          'theme_location' => 'my-custom-menu'
        );
            wp_nav_menu($args);

        ?>
      </div>
    </div>
  </div>
</nav>
