<?php
/**
 * Template Name: About
 */
get_header();
?>
  <section class="about-services">
    <div class="wrapper">
      <div class="icons-box">
        <div class="icon">
          <img src="<?php the_field('icon1_image'); ?>" alt="">
          <h2><?php the_field('icon1_content'); ?></h2>
        </div>
        <div class="icon">
          <img src="<?php the_field('icon1_image'); ?>" alt="">
          <h2><?php the_field('icon2_content'); ?></h2>
        </div>
        <div class="icon">
          <img src="<?php the_field('icon1_image'); ?>" alt="">
          <h2><?php the_field('icon3_content'); ?></h2>
        </div>
      </div>
    </div>
  </section>
  <section class="about-content">
    <div class="wrapper">
      <h1><img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" alt="">About us</h1>
      <div class="about-content-box">
        <p>
          <?php the_field('content_paragraph_1'); ?>
        </p>
        <p>
          <?php the_field('content_paragraph_2'); ?>
        </p>
        <p>
          <?php the_field('content_paragraph_3'); ?>
        </p>
        <p>
          <?php the_field('content_paragraph_4'); ?>
        </p>
      </div>
      <div class="about-contact-box">
        <p><?php the_field('adress_line_1'); ?></p>
        <p><?php the_field('adress_line_2'); ?></p>
        <p><?php the_field('adress_line_3'); ?></p>
        <p><?php the_field('adress_line_4'); ?></p>
        <a href="<?php the_field('adress_line_5_link'); ?>"><?php the_field('adress_line_5'); ?></a>
        <div class="contact-box-button">
          <a href="<?php the_field('contact_button_link'); ?>">Contact us</a>
        </div>
      </div>
    </div>
  </section>
  <?php get_footer();  ?>
