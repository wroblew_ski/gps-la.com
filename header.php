<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <title>GPS</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700&amp;subset=latin-ext" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Hind:500,700&amp;subset=latin-ext" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Cabin:600,700&amp;subset=latin-ext" rel="stylesheet">
  <link rel="stylesheet" href="css/style.css">
  <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAiDZpVTns56M1aJNUF3V3yBVAroEpuOt0"></script>
  <?php wp_head(); ?>
</head>

<body>
  <header>
    <?php
    include('partials/mainNav.php');
    ?>
  </header>
