<?php
/**
 * Template Name: Single Service
 */
get_header();
?>
  <div class="single-service-header" style="background-image: url('<?php the_field('single_service_background'); ?>')">
    <h1><?php the_field('single_service_title'); ?></h1>
  </div>
  <div class="wrapper">
    <section class="single-service-content">
      <aside>
          <?php include('partials/serviceNav.php'); ?>
      </aside>
      <div class="content-box">
        <h2><?php the_field('single_service_title'); ?></h2>
        <div><?php the_field('single_service_paragraph_1'); ?></div>
        <div><?php the_field('single_service_paragraph_2'); ?></div>
        <div><?php the_field('single_service_paragraph_3'); ?></div>
        <div>
          <?php the_field('single_service_paragraph_4'); ?>
        </div>
        <div><?php the_field('single_service_paragraph_5'); ?></div>
        <div><?php the_field('single_service_paragraph_6'); ?></div>
        <div class="service-box-button">
          <a href="<?php echo get_page_link(14); ?>">Back to Services page</a>
        </div>
      </div>
    </section>
  </div>
<?php get_footer() ?>
