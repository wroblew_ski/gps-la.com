<?php
function img_unautop($pee) {
    $pee = preg_replace('/<p>\\s*?(<a .*?><img.*?><\\/a>|<img.*?>)?\\s*<\\/p>/s', '<div class="figure">$1</div>', $pee);
    return $pee;
}

function gps_scripts() {
	wp_enqueue_style( 'slick', get_theme_file_uri( '/slick/slick.css')  );
	wp_enqueue_style( 'gps-style', get_theme_file_uri( '/css/style.css')  );

	wp_enqueue_script( 'slickjs', get_theme_file_uri( '/slick/slick.js' ), array( 'jquery' ), '1.0', true );
	wp_enqueue_script( 'global', get_theme_file_uri( '/js/scripts.js' ), array( 'jquery' ), '1.0', true );
}
add_action( 'wp_enqueue_scripts', 'gps_scripts' );

function wpb_custom_new_menu() {
  register_nav_menu('my-custom-menu',__( 'Header Menu' ));
  register_nav_menu('service-menu',__( 'Service Menu' ));
  register_nav_menu('main-nav',__( 'Main Navbar without dropdown' ));
  register_nav_menu('footer-nav',__( 'Footer nav' ));
}
add_action( 'init', 'wpb_custom_new_menu' );
//Remove WPAUTOP from ACF TinyMCE Editor
function acf_wysiwyg_remove_wpautop() {
    remove_filter('acf_the_content', 'wpautop' );
}
add_action('acf/init', 'acf_wysiwyg_remove_wpautop');

//
function the_content_without_filters( $the_content=null ) {
    remove_filter('the_content', 'wpautop');
    if( $the_content ) {
      the_content( $the_content );
    } else {
      the_content();
    }
    add_filter('the_content', 'wpautop');
  }

  function the_excerpt_without_filters( $the_excerpt=null ) {
    remove_filter('the_excerpt', 'wpautop');
    if( $the_excerpt ) {
      the_excerpt( $the_excerpt ) ;
    } else {
      the_excerpt();
    }
    add_filter('the_excerpt', 'wpautop');
  }

  function the_field_without_filters( $the_field=null ) {
    remove_filter('acf_the_content', 'wpautop');
    if( $the_field ) {
      the_field( $the_field );
    } else {
      the_field();
    }
    add_filter('acf_the_content', 'wpautop');
  }

?>
