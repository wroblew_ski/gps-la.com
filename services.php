<?php
/**
 * Template Name: Services
 */
get_header();
?>
  <section class="services">
    <div class="wrapper">
      <div class="service-box">
        <div class="image">
          <img src="<?php the_field('service_1_image'); ?>" alt="">
        </div>
        <div class="content">
          <?php the_field('service_1_content'); ?>
          <div class="about-button">
            <a href="<?php the_field('service_1_link'); ?>">Read more</a>
          </div>
        </div>
      </div>
      <div class="service-box">
        <div class="image">
          <img src="<?php the_field('service_2_image'); ?>" alt="">
        </div>
        <div class="content">
          <?php the_field('service_2_content'); ?>
          <div class="about-button">
            <a href="<?php the_field('service_2_link'); ?>">Read more</a>
          </div>
        </div>
      </div>
      <div class="service-box">
        <div class="image">
          <img src="<?php the_field('service_3_image'); ?>" alt="">
        </div>
        <div class="content">
          <?php the_field('service_3_content'); ?>
          <div class="about-button">
            <a href="<?php the_field('service_3_link'); ?>">Read more</a>
          </div>
        </div>
      </div>
      <div class="service-box">
        <div class="image">
          <img src="<?php the_field('service_4_image'); ?>" alt="">
        </div>
        <div class="content">
          <?php the_field('service_4_content'); ?>
          <div class="about-button">
            <a href="<?php the_field('service_4_link'); ?>">Read more</a>
          </div>
        </div>
      </div>
      <div class="service-box">
        <div class="image">
          <img src="<?php the_field('service_5_image'); ?>" alt="">
        </div>
        <div class="content">
          <?php the_field('service_5_content'); ?>
          <div class="about-button">
            <a href="<?php the_field('service_5_link'); ?>">Read more</a>
          </div>
        </div>
      </div>
    </div>
  </section>
<?php get_footer() ?>
