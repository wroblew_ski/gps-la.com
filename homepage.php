<?php
/**
 * Template Name: Homepage
 */
  get_header();
 ?>
  <section class="homepage-header">
    <div class="slider-progress">
      <div class="progress"></div>
    </div>
    <div class="slider-box">
      <div class="slide" style="background-image: url('<?php the_field('slide1background'); ?>')">
        <div class="wrapper">
          <div class="slide-content">
            <?php the_field('slide1content'); ?>
          </div>
        </div>
      </div>
      <div class="slide" style="background-image: url('<?php the_field('slide2background'); ?>')">
        <div class="wrapper">
          <div class="slide-content">
            <?php the_field('slide2content'); ?>
          </div>
        </div>
      </div>
      <div class="slide" style="background-image: url('<?php the_field('slide3background'); ?>')">
        <div class="wrapper">
          <div class="slide-content">
            <?php the_field('slide3content'); ?>
          </div>
        </div>
      </div>
      <div class="slide" style="background-image: url('<?php the_field('slide4background'); ?>')">
        <div class="wrapper">
          <div class="slide-content">
            <?php the_field('slide4content'); ?>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="homepage-content">
    <div class="homepage-content-box">
      <div class="spacer-top"></div>
      <div class="content-box">
        <div class="single-box">
          <?php the_field('box1'); ?>
          <div class="homepage-box-button">
            <a href="<?php the_field('box1link'); ?>">Read more</a>
          </div>
        </div>
        <div class="single-box">
          <?php the_field('box2'); ?>
          <div class="homepage-box-button">
            <a href="<?php the_field('box2link'); ?>">Read more</a>
          </div>
        </div>
        <div class="single-box">
          <?php the_field('box3'); ?>
          <div class="homepage-box-button">
            <a href="<?php the_field('box3link'); ?>">Read more</a>
          </div>
        </div>
        <div class="single-box">
          <?php the_field('box4'); ?>
          <div class="homepage-box-button">
            <a href="<?php the_field('box4link'); ?>">Read more</a>
          </div>
        </div>
      </div>
      <div class="spacer-bottom"></div>
    </div>
  </section>
  <?php get_footer(); ?>
